# CKS Preparation

Currently I prepare for the CKS Exam (v1.23) and will write down some notes and practical examples here.
Most of them will work on every Kubernetes distribution, however since the exam relies on a Kubeadm installation I recommend [minikube](https://kind.sigs.k8s.io) or [kind](https://kind.sigs.k8s.io)  for a quick cluster setup, as this is very close.

