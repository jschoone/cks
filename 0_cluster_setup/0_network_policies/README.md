# Network Policies

To use [Network Policies](https://kubernetes.io/docs/concepts/services-networking/network-policies/) a proper network plugin is needed. Flannel, which is mostly used on lightweight clusters won't work here.

I will use [Cilium](https://cilium.io) here.

```
minikube start -n2 --cni cilium
```

To get a feeling for Network Policies, I will start with only two namespaces `blue` and `green`.

`kubectl apply -f 0_namespaces.yaml`

There are nginx pods running in both namespaces with exposed ports to make them available using the cluster DNS.
With that it can be checked if the Network Policies do what they should.

`kubectl apply -f 1_pods.yaml`

All traffic is allowed by default. The easiest way to check the connection is using a `curl` pod inside the cluster.
Since the traffic will be restricted between `blue` and `green` the pod will run from the blue namespace.
The pod will be reached via the exposed service using `<service>.<namespace>`

```
kubectl run curl --image=alpine/curl --namespace blue --rm -ti -- curl nginx.green
kubectl run curl --image=alpine/curl --namespace blue --rm -ti -- curl nginx2.green
kubectl run curl --image=alpine/curl --namespace blue --rm -ti -- curl nginx.blue
```

All commands should return the `Welcome to nginx` stuff.

Now the traffic will be restricted using Network Policies.

`kubectl apply -f 2_netpol.yaml`

This manifest file includes four Network Policies. First all traffic TO the green namespace and FROM the blue namespace will be denied.
Then traffic TO the `nginx-green` pod FROM blue namespace will be allowed. `nginx2-green` should still be denied.
Also the traffic FROM the blue namespace to the green namespace will be allowed as long as traffic to all pods labeled with k8s-app=kube-dns in the kube-system namespace, the IP address 8.8.8.8 to test the `ipBlock` functionality and traffic to port 53 to be able to make DNS requests.
