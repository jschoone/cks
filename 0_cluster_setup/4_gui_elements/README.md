# RBAC for dashboard

[Kubernetes Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)

### Install Kubernetes Dashboard

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.5.0/aio/deploy/recommended.yaml
```

### Connect ClusterRole `view` to ServiceAccount `kubernetes-dashboard`
```
kubectl -n kubernetes-dashboard create rolebinding insecure --serviceaccount kubernetes-dashboard:kubernetes-dashboard --clusterrole view
```

### Browse to dashboard and search for `kubernetes-dashboard` in Namespace-Selector

Create port-forward to dashboard service
```
kubectl port-forward -n kubernetes-dashboard services/kubernetes-dashboard 8443:443
```

Then browse `https://localhost:8443`

Get the Token
```
kubectl get secrets -n kubernetes-dashboard $(kubectl get sa -n kubernetes-dashboard kubernetes-dashboard -o jsonpath='{ .secrets[0].name }') -o jsonpath='{ .data.token }' | base64 -d
```

The ServiceAccount should have access to the namespace `kubernetes-dashboard` which can be selected above.

Add cluster wide `view` access
`k -n kubernetes-dashboard create clusterrolebinding insecure --serviceaccount kubernetes-dashboard:kubernetes-dashboard --clusterrole view`

Now the ServiceAccount has access to all namespaces.
