## CIS Benchmarks

### Search CIS benchmarks

https://www.cisecurity.org/benchmark/kubernetes
Download the PDF

### kube-bench

https://github.com/aquasecurity/kube-bench/blob/main/docs/running.md

#### With Docker
`docker run --pid=host -v /etc:/etc:ro -v /var:/var:ro -t aquasec/kube-bench:latest --version 1.23.4`
`docker run --pid=host -v /etc:/etc:ro -v /var:/var:ro -v $(which kubectl):/usr/local/mount-from-host/bin/kubectl -v ~/.kube:/.kube -e KUBECONFIG=/.kube/config -t aquasec/kube-bench:latest`

#### With Kubernetes
```
git clone https://github.com/aquasecurity/kube-bench.git
cd kube-bench
kubectl apply -f job.yaml
kubectl logs $(kubectl get pods --selector=app=kube-bench -o jsonpath='{ .items[0].metadata.name }')
```

Check for FAIL
