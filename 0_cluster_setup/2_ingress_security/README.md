# Ingress Security

[Ingress documentation](https://kubernetes.io/docs/concepts/services-networking/ingress/)

Ingress Controllers are Layer-7-LoadBalancers or ReverseProxies in a Kubernetes cluster.
With that Pods can be made accessible using vhosts like we know from good old Apache httpd.
In Kubernetes we don't really have to worry about the Ingress controller, we just need to create Ingress resources and mention a class name to pick the correct one.
In this case the Ingress resource will be configured to use TLS certificates to secure the connection via HTTPS.

### Kubernetes setup

A minikube with default configuration will be enough.

`minikube start`

### Install ingress-nginx

Assuming there is no Helm and Minikube Addons, the Ingress Controller will be installed using the manifests

```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.2.0/deploy/static/provider/baremetal/deploy.yaml
```

### Create the Pods

There will be two Pods, `httpd` runs an httpd server and `nginx` runs an nginx server, surprise!

```
kubectl apply -f 0_pods.yaml
```

### Create certificates

As usual we need a certificate and a key as usual when configuring a webserver.
For demonstration it's enough to create a self-signed cert here.

```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout webserver.key -out webserver.crt -subj /CN=webserver.example.org
```

### Create secret from certificates

```
kubectl create secret tls webserver-tls --key webserver.key --cert webserver.crt
```

### Add Ingress resource

```
kubectl apply -f 1_ingress.yaml
```

### Check the certificates

Best way to check if the Ingress controller uses the `webserver-tls` certificate is to use curl.
Since the Ingress resource is configured for the host `webserver.example.org` it's not enough to curl the IP-Address of the cluster, the request must be against the correct domain.
One possible way to achieve this is to add the IP-Address of any node to the `/etc/hosts` file along with the domain.
But curl can do it as well with the `--resolve` option.

Let's save the IP address for the first appearing node in `KUBE_IP` and the Ingress HTTPS NodePort to `INGRESS_HTTPS_NODEPORT`
```
export KUBE_IP=$(kubectl get nodes -o jsonpath='{ .items[0].status.addresses[?(@.type=="InternalIP")].address }')
export INGRESS_HTTPS_NODEPORT=$(kubectl get svc -n ingress-nginx ingress-nginx-controller -o jsonpath='{ .spec.ports[?(@.appProtocol=="https")].nodePort }')
curl -vk --resolve webserver.example.org:$INGRESS_HTTPS_NODEPORT:$KUBE_IP https://webserver.example.org:$INGRESS_HTTPS_NODEPORT/httpd
```
